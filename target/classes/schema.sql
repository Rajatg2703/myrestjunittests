drop table if exists course;
create table course
(
    id              bigint(20) not null auto_increment,
    subcode         varchar(25) not null,
    subname         varchar(50) not null,
    description     varchar(500) ,
    prereq          varchar(250) ,
    primary key(id)
);

drop table if exists syllabus;
create table syllabus
(
    id 		        bigint(20) not null auto_increment,
    subcode 	    varchar(25) not null,
    uno 	        varchar(10) not null,
    uname  		    varchar(100) not null,
    ucontent 	    varchar(1000) not null,
    course_id 	    bigint(20) references course(id) on delete cascade,
    primary key(id)
);