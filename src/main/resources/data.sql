insert ignore into course (id, subcode, subname, description, prereq) values
  (1, 'cs1001', 'English', 'editor@nexus.corp', 'Basic elementary knowledge'),
  (2, 'cs1002', 'Chemistry', 'stking@darktower.it','Basic till 12th standard'),
  (3, 'cs1003', 'Math', 'john.doe@acme.io', 'knowledge till 12th standard');

insert ignore into syllabus (id, subcode, uno, uname, ucontent, course_id)
 values
  (1, 'cs1001', '1', 'Sci-Fi', 'jmcakdfmansdvjaffadnalkfjshdajnc cxz',1),
  (2, 'cs1001', '2', 'Sci-Fii', 'sdakjnjkxnmzcvneijf vanfueic iae',1),
  (3, 'cs1001', '3', 'Sci-Fiii', 'kasxzncvmneifn cve v ncuef cuerfiu viern vienuiv',1),
  (4, 'cs1002', '1', 'chemistri', 'dsnckdjasnfieuia nvfuanceufancmxzcnvmznveauifv znafnqfalkjdnaldfaofj',2),
  (5, 'cs1002', '2', 'chemistri', 'jfsdlnvieuhfnqhruqorhfncwruonfcsmxfwoero',2);