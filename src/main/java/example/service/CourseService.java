package example.service;

import example.dto.CourseDto;
import example.dto.SyllabusDto;
import example.exception.DuplicatedEntityException;
import example.exception.EntityNotFoundException;
import example.model.Course;
import example.model.Syllabus;
import example.repository.CourseRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class CourseService {

    private final CourseRepository courseRepository;
    private final ModelMapper modelMapper;

    @Autowired
    public CourseService(CourseRepository courseRepository, ModelMapper modelMapper) {
        this.courseRepository = courseRepository;
        this.modelMapper = modelMapper;
    }
    @Transactional
    public CourseDto get(Long courseId) {
        Course course = courseRepository.findById(courseId)
                .orElseThrow(EntityNotFoundException::new);
        return toDto(course);
    }

    @Transactional
    public Long create(CourseDto courseDto) {

        if (courseDto.getId()!=null) {
            courseRepository.findById(courseDto.getId()).ifPresent(course -> {
                throw new DuplicatedEntityException("Entity Course with id " + course.getId() + " already exists");
            });
        }

        Course course = modelMapper.map(courseDto, Course.class);
        return courseRepository.save(course).getId();
    }

    @Transactional
    public CourseDto delete(Long courseId) {

        Course course = courseRepository.findById(courseId).orElseThrow(EntityNotFoundException::new);
        courseRepository.delete(course);
        return modelMapper.map(course, CourseDto.class);
    }
    private CourseDto toDto(Course course) {

        return modelMapper.map(course, CourseDto.class);
    }

}
