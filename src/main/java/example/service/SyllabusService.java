package example.service;

import java.util.List;
import java.util.stream.Collectors;

import example.dto.CourseDto;
import example.dto.SyllabusDto;
import example.exception.DuplicatedEntityException;
import example.exception.EntityNotFoundException;
import example.model.Course;
import example.model.Syllabus;
import example.repository.SyllabusRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class SyllabusService {

    private final SyllabusRepository syllabusRepository;
    private final ModelMapper modelMapper;

    @Autowired
    public SyllabusService(SyllabusRepository syllabusRepository, ModelMapper modelMapper) {
        this.syllabusRepository = syllabusRepository;
        this.modelMapper = modelMapper;
    }

    @Transactional
    public Long create(SyllabusDto syllabusDto) {

        if (syllabusDto.getId()!=null) {
            syllabusRepository.findById(syllabusDto.getId()).ifPresent(Syllabus -> {
                throw new DuplicatedEntityException("Entity Syllabus with id " + Syllabus.getId() + " alreSyllabusy exists");
            });
        }

        Syllabus syllabus = modelMapper.map(syllabusDto, Syllabus.class);
        return syllabusRepository.save(syllabus).getId();
    }

    @Transactional(readOnly=true)
    public SyllabusDto get(Long syllabusId) {
        Syllabus syllabus = syllabusRepository.findById(syllabusId)
                .orElseThrow(EntityNotFoundException::new);
        return toDto(syllabus);
    }

    @Transactional(readOnly=true)
    public List<SyllabusDto> list() {
        return syllabusRepository.findAll()
                .stream()
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    @Transactional
    public SyllabusDto delete(Long syllabusId) {

        Syllabus syllabus = syllabusRepository.findById(syllabusId).orElseThrow(EntityNotFoundException::new);
        syllabusRepository.delete(syllabus);
        return modelMapper.map(syllabus, SyllabusDto.class);
    }

    private SyllabusDto toDto(Syllabus syllabus) {

        return modelMapper.map(syllabus, SyllabusDto.class);
    }

}
