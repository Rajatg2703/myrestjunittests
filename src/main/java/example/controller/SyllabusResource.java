package example.controller;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import example.dto.SyllabusDto;
import example.service.SyllabusService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@Api(value="Syllabus Resource Endpoint")
@RestController
@RequestMapping("/api/v1")
@Slf4j
public class SyllabusResource {

    private final SyllabusService syllabusService;

    @Autowired
    public SyllabusResource(SyllabusService syllabusService) {
        this.syllabusService = syllabusService;
    }

    @ApiOperation(value = "Gets the list of available Syllabus")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = List.class)
    })
    @GetMapping(path = "/syllabus", produces = {"application/json"})
    public ResponseEntity<List<SyllabusDto>> list() {

//        log.info("GET /api/v1/syllabus");
        return ResponseEntity.ok(syllabusService.list());
    }

    @ApiOperation(value = "Retrieves the requested syllabus")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = SyllabusDto.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 422, message = "Unprocessable Entity"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @GetMapping(path = "/syllabus/{id}", produces = {"application/json"})
    public ResponseEntity<SyllabusDto> get(@PathVariable("id") Long syllabusId) {

        log.info("GET /api/v1/syllabus/"+syllabusId);
        return ResponseEntity.ok(syllabusService.get(syllabusId));
    }

    @ApiOperation(value = "Creates a new Syllabus")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created"),
            @ApiResponse(code = 400, message = "Syllabus Request"),
            @ApiResponse(code = 422, message = "Unprocessable Entity"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping(path = "/syllabus", consumes = {"application/json"})
    public ResponseEntity<Void> create(@Valid @RequestBody SyllabusDto syllabusDto) {

        log.info("POST /api/v1/syllabus : "+syllabusDto);
        Long syllabusId = syllabusService.create(syllabusDto);

        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(syllabusId).toUri();

        return ResponseEntity.created(location).build();
    }
    @ApiOperation(value = "Removes the requested Course and Syllabus")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Deleted"),
            @ApiResponse(code = 204, message = "No Content"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @DeleteMapping(path = "/syllabus/{id}", produces = {"application/json"})
    public ResponseEntity<Void> delete(@PathVariable("id") Long syllabusId) {

        log.info("DELETE /api/v1/syllabus/"+syllabusId);
        syllabusService.delete(syllabusId);
        return ResponseEntity.ok().build();
    }
}
