package example.controller;

import example.dto.CourseDto;
import example.dto.SyllabusDto;
import example.service.CourseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;

@Api(value="Course Resource Endpoint")
@RestController
@RequestMapping("/api/v1")
@Slf4j
public class CourseResource {

    private final CourseService courseService;

    @Autowired
    public CourseResource(CourseService courseService) {
        this.courseService = courseService;
    }

    @ApiOperation(value = "Retrieves the requested course")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = CourseDto.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 422, message = "Unprocessable Entity"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @GetMapping(path = "/courses", consumes = {"application/json"})
    public ResponseEntity <CourseDto> get(@PathVariable("id") Long courseId){
        log.info("GET /api/v1/courses/"+courseId);
        return ResponseEntity.ok(courseService.get(courseId));
    }

    @ApiOperation(value = "Creates a new Course")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 422, message = "Unprocessable Entity"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })

    @PostMapping(path = "/courses", consumes = {"application/json"})
    public ResponseEntity<Void> create(@Valid @RequestBody CourseDto courseData) {

        log.info("POST /api/v1/courses :"+courseData);
        long adId = courseService.create(courseData);

        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(adId).toUri();

        return ResponseEntity.created(location).build();
    }

    @ApiOperation(value = "Removes the requested Course")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "No Content"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @DeleteMapping(path = "/courses/{id}", produces = {"application/json"})
    public ResponseEntity<Void> delete(@PathVariable("id") Long courseId) {


        log.info("DELETE /api/v1/courses/"+courseId);
        courseService.delete(courseId);
        return ResponseEntity.noContent().build();
    }
}
