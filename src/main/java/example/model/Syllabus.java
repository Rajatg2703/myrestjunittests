package example.model;

import lombok.*;

import javax.persistence.*;

import java.math.BigDecimal;

import static javax.persistence.GenerationType.IDENTITY;


@Entity
@Table(name = "syllabus")
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@EqualsAndHashCode(of = {"id"})
public class Syllabus {

    @Id
    @Column(name = "id", unique = true, updatable = false, nullable = false)
    @GeneratedValue(strategy = IDENTITY)
    private Long id;

    @Column(name = "subcode", nullable = false)
    private String subcode;

    @Column(name = "uno")
    private String uno;

    @Column(name = "uname", nullable = false)
    private String uname;

    @Column(name = "ucontent")
    private String ucontent;

    @ManyToOne(optional = false)
    private Course course;
}
