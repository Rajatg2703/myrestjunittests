package example.model;

import lombok.*;
import javax.persistence.*;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "course")
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@EqualsAndHashCode(of = {"id"})
public class Course {
    @Id
    @Column(name = "id", unique = true, updatable = false, nullable = false)
    @GeneratedValue(strategy = IDENTITY)
    private Long id;

    @Column(name = "subcode", nullable = false)
    private String subcode;

    @Column(name = "subname" , nullable = false)
    private String subname;

    @Column(name = "description" , nullable = false)
    private String description;

    @Column(name = "prereq" )
    private String prereq;


    @OneToMany(mappedBy = "course", cascade = CascadeType.REMOVE)
    private List<Syllabus> syllabus;

}
