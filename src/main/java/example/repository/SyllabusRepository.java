package example.repository;

import example.model.Syllabus;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SyllabusRepository extends JpaRepository<Syllabus,Long> {
    Long deleteBySubcode(String subcode);
}
