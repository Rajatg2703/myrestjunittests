package example.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class SyllabusDto {

    private Long id;

    @NotBlank(message = "Subject code is mandatory")
    private String subcode;

    @NotBlank(message = "Unit number is mandatory")
    private String uno;

    @NotNull(message = "Unit name is mandatory")
    private String uname;

    @NotBlank(message = "Content is mandatory")
    private String ucontent;

    @NotNull(message = "Course is mandatory")
    private CourseDto course;
}
