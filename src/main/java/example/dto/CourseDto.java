package example.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class CourseDto {

    private Long id;

    @NotBlank(message = "Subject code is mandatory")
//    @Pattern(regexp = "^[A-Za-z0-9]+$", message = "An alphanumeric name is mandatory")
    private String subcode;

    @NotBlank(message = "Subject name is mandatory")
    private String subname;

    @NotBlank(message = "Description about book")
    //@Email(message = "A valid mail address is mandatory")
    private String description;

    @NotBlank(message = "Prerequsite for book")
    private String prereq;
}
