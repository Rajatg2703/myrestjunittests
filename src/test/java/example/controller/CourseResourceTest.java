package example.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import example.dto.CourseDto;
import example.exception.DuplicatedEntityException;
import example.exception.EntityNotFoundException;
import example.service.CourseService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
@Tag("IntegrationTest")
@DisplayName("Course Resource Integration Tests")
public class CourseResourceTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private CourseService courseService;

    @Test
    @DisplayName("create Course, should return expected 201")
    public void createCourseShouldReturn201() throws Exception {

        //given
        CourseDto courseDto = CourseDto.builder().id(0l).subcode("cs101").subname("english").description("abbadabbachaabba").prereq("bcb").build();//.id(1l)
        String json = objectMapper.writeValueAsString(courseDto);
        when(this.courseService.create(courseDto)).thenReturn(0L);

        //when-then
        this.mockMvc.perform(post("/api/v1/courses/")
                .contentType(APPLICATION_JSON_UTF8)
                .content(json))
                .andExpect(status().isCreated());
    }

    @Test
    @DisplayName("create incomplete Course, should return 400")
    public void createIncompleteCourseShouldReturn400() throws Exception {

        //given
        CourseDto courseDto = CourseDto.builder().id(5l).subname("english").description("abbadabbachaabba").prereq("bcb").build();
        String json = objectMapper.writeValueAsString(courseDto);

        //when-then
        this.mockMvc.perform(post("/api/v1/courses/")
                .contentType(APPLICATION_JSON_UTF8)
                .content(json))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.timestamp", is(notNullValue())))
                .andExpect(jsonPath("$.errors").isArray())
                .andExpect(jsonPath("$.errors", hasSize(1)))
                .andExpect(jsonPath("$.errors", hasItem("Subject code is mandatory")));
//                .andExpect(jsonPath("$.errors", hasItem("Subject name is mandatory")))
//                .andExpect(jsonPath("$.errors", hasItem("Description about book")))
//                .andExpect(jsonPath("$.errors", hasItem("Prerequsite for book")));
    }

    @Test
    @DisplayName("create existing Course id, should return 422")
    public void createExistingCourseIdShouldReturn404() throws Exception {

        //given
        long existingCourseId = 0L;
        CourseDto course1 = CourseDto.builder().id(existingCourseId).subcode("cs101").subname("english").description("abbadabbachaabba").prereq("bcb").build();
        String json = objectMapper.writeValueAsString(course1);
        when(this.courseService.create(course1)).thenThrow(new DuplicatedEntityException());

        //when-then
        this.mockMvc.perform(post("/api/v1/courses/")
                .contentType(APPLICATION_JSON_UTF8)
                .content(json))
                .andExpect(status().isUnprocessableEntity());
    }

    @Test
    @DisplayName("delete Course, should return expected Course Id")
    public void deleteCourseShouldReturn204() throws Exception {

        //given
        long existingCourseId = 0L;
        when(this.courseService.delete(existingCourseId)).thenReturn(null);

        //when-then
        this.mockMvc.perform(delete("/api/v1/courses/"+existingCourseId))
                .andExpect(status().isNoContent());
    }

    @Test
    @DisplayName("delete non existing Course id, should return 404")
    public void deleteExistingCourseIdShouldReturn404() throws Exception {

        //given
        long nonExistingCourseId = 404L;
        when(this.courseService.delete(nonExistingCourseId)).thenThrow(new EntityNotFoundException());

        //when-then
        this.mockMvc.perform(delete("/api/v1/courses/"+nonExistingCourseId))
                .andExpect(status().isNotFound());
    }
}
