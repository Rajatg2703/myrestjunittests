package example.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import example.dto.CourseDto;
import example.exception.DuplicatedEntityException;
import example.exception.EntityNotFoundException;
import example.dto.SyllabusDto;
import example.service.SyllabusService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;



import java.math.BigDecimal;
import java.util.ArrayList;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
@Tag("IntegrationTest")
@DisplayName("Syllabus Resource Integration Tests")
public class SyllabusResourceTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private SyllabusService syllabusService;

    @Test
    @DisplayName("get Syllabus, should return expected Syllabus")
    public void getSyllabusShouldReturn200() throws Exception {

        //given
        long syllabusId = 0L;
        given(this.syllabusService.get(syllabusId)).willReturn(new SyllabusDto());

        //when-then
        this.mockMvc.perform(get("/api/v1/syllabus/"+syllabusId)
                .accept(APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName("get Syllabus list, should return complete Syllabus list")
    public void getSyllabusListShouldReturn200() throws Exception {

        //given
        given(this.syllabusService.list()).willReturn(new ArrayList<>());

        //when-then
        this.mockMvc.perform(get("/api/v1/syllabus")
                .accept(APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName("get non existing Syllabus, should return 404")
    public void getNotExistingSyllabusShouldReturn404() throws Exception {

        //given
        long nonExistentSyllabusId = 404L;
        given(this.syllabusService.get(nonExistentSyllabusId))
                .willThrow(new EntityNotFoundException());

        //when-then
        this.mockMvc.perform(get("/api/v1/syllabus/"+nonExistentSyllabusId))
                .andExpect(status().isNotFound());
    }

    @Test
    @DisplayName("create Syllabus, should return 201")
    public void createSyllabusShouldReturn201() throws Exception {

        //given
        CourseDto defaultCourse = CourseDto.builder().id(0L).build();
        SyllabusDto syllabus = SyllabusDto.builder().id(0l).subcode("cs1001").uno("1").uname("abc").ucontent("aklsjfajkasfjasdhfliuncjkncauie").course(defaultCourse).build();
        String json = objectMapper.writeValueAsString(syllabus);
        when(this.syllabusService.create(syllabus)).thenReturn(0L);

        //when-then
        this.mockMvc.perform(post("/api/v1/syllabus/")
                .contentType(APPLICATION_JSON_UTF8)
                .content(json))
                .andExpect(status().isCreated());
    }

    @Test
    @DisplayName("create existing Syllabus id, should return 422")
    public void createExistingSyllabusIdShouldReturn422() throws Exception {

        //given
        long existingSyllabusId = 0L;
        CourseDto defaultCourse = CourseDto.builder().id(existingSyllabusId).build();
        SyllabusDto syllabus1 = SyllabusDto.builder().subcode("cs1001").uno("1").uname("abc").ucontent("aklsjfajkasfjasdhfliuncjkncauie").course(defaultCourse).build();
        String json = objectMapper.writeValueAsString(syllabus1);
        when(this.syllabusService.create(syllabus1)).thenThrow(new DuplicatedEntityException());

        //when-then
        this.mockMvc.perform(post("/api/v1/syllabus/")
                .contentType(APPLICATION_JSON_UTF8)
                .content(json))
                .andExpect(status().isUnprocessableEntity());
    }

    @Test
    @DisplayName("create incomplete Syllabus, should return 400")
    public void createIncompleteSyllabusShouldReturn400() throws Exception {

        //given
        SyllabusDto syllabus1 = SyllabusDto.builder().id(6l).subcode("cs1001").uno("1").ucontent("aklsjfajkasfjasdhfliuncjkncauie").build();
        String json = objectMapper.writeValueAsString(syllabus1);

        //when-then
        this.mockMvc.perform(post("/api/v1/syllabus/")
                .contentType(APPLICATION_JSON_UTF8)
                .content(json))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.timestamp", is(notNullValue())))
                .andExpect(jsonPath("$.errors").isArray())
                .andExpect(jsonPath("$.errors", hasSize(2)))
                .andExpect(jsonPath("$.errors", hasItem("Unit name is mandatory")))
                .andExpect(jsonPath("$.errors", hasItem("Course is mandatory")));
    }


}
