package example.controller;

import example.Application;
import example.dto.CourseDto;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.HttpMethod.DELETE;
import static org.springframework.http.HttpStatus.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = RANDOM_PORT, classes = Application.class)
@ActiveProfiles("test")
@DisplayName("CourseResource REST API Tests")
@Tag("IntegrationTest")
public class CourseResourceRestTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    @DisplayName("when POST a new Course, then returns 201")
    public void givenNewCourse_whenPostCourse_thenReturns201() {

        //given
        HttpEntity<CourseDto> request = new HttpEntity<>(CourseDto.builder().id(6l).subcode("cs101").subname("english").description("abbadabbachaabba").prereq("bcb").build());

        //when
        ResponseEntity<Void> responseEntity = restTemplate.postForEntity("/api/v1/courses", request, Void.class);

        //then
        assertEquals(CREATED, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getHeaders().getLocation());
    }

    @Test
    @DisplayName("when POST an existing Course id, then returns 422")
    public void givenExistingCourseId_whenPostCourse_thenReturns422() {

        //given
        Long existingCourseId = 1L;
        HttpEntity<CourseDto> request = new HttpEntity<>(CourseDto.builder().id(existingCourseId).subcode("cs101").subname("english").description("abbadabbachaabba").prereq("bcb").build());

        //when
        ResponseEntity<Void> responseEntity = restTemplate.postForEntity("/api/v1/courses", request, Void.class);

        //then
        assertEquals(UNPROCESSABLE_ENTITY, responseEntity.getStatusCode());
    }

    @Test
    @DisplayName("when DELETE a new Course, then returns 204")
    public void givenExistingCourseId_whenDeleteCourse_thenReturns204() {

        //given
        Long existingCourseId = 2L;
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<String> entity = new HttpEntity<>(headers);

        //when
        ResponseEntity<String> responseEntity = restTemplate.exchange("/api/v1/courses/" + existingCourseId, DELETE, entity, String.class);

        //then
        assertEquals(NO_CONTENT, responseEntity.getStatusCode());
    }

    @Test
    @DisplayName("when DELETE an existing Course id, then returns 404")
    public void givenNonExistingCourseId_whenPostCourse_thenReturns422() {

        //given
        Long existingCourseId = 404L;
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<String> entity = new HttpEntity<>(headers);

        //when
        ResponseEntity<String> responseEntity = restTemplate.exchange("/api/v1/course/" + existingCourseId, DELETE, entity, String.class);

        //then
        assertEquals(NOT_FOUND, responseEntity.getStatusCode());
    }


}
