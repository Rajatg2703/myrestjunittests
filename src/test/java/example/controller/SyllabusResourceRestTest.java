package example.controller;
import example.Application;
import example.dto.CourseDto;
import example.dto.SyllabusDto;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import java.math.BigDecimal;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.HttpStatus.*;
import static org.springframework.http.HttpStatus.UNPROCESSABLE_ENTITY;
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = RANDOM_PORT, classes = Application.class)
@ActiveProfiles("test")
@DisplayName("Syllabus Resource REST API Tests")
@Tag("IntegrationTest")
public class SyllabusResourceRestTest {
    @Autowired
    private TestRestTemplate restTemplate;
    @Test
    @DisplayName("when GET Syllabus list, then returns 200")
    public void whenGetSyllabusList_thenReturns200() {
        //when
        ResponseEntity<List> responseEntity = restTemplate.getForEntity("/api/v1/syllabus", List.class);
        //then
        assertEquals(OK, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
        assertFalse(responseEntity.getBody().isEmpty());
    }
    @Test
    @DisplayName("given Syllabus id, when GET existing Syllabus, then returns 200")
    public void givenSyllabusId_whenGetNonExistingSyllabus_thenReturns200() {
        //given
        Long syllabus1Id = 1L;
        //when
        ResponseEntity<SyllabusDto> responseEntity = restTemplate.getForEntity("/api/v1/syllabus/"+syllabus1Id, SyllabusDto.class);
        //then
        assertEquals(OK, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
        assertNotNull(responseEntity.getBody().getId());
        assertNotNull(responseEntity.getBody().getSubcode());
        assertNotNull(responseEntity.getBody().getUcontent());
        assertNotNull(responseEntity.getBody().getUname());
        assertNotNull(responseEntity.getBody().getUno());
    }
    @Test
    @DisplayName("given Syllabus id, when GET non existing Syllabus, then returns 404")
    public void givenNonExistentSyllabusId_whenGetNonExistingSyllabus_thenReturns404() {
        //given
        Long syllabus1Id = 404L;

        //when
        ResponseEntity<SyllabusDto> responseEntity = restTemplate.getForEntity("/api/v1/syllabus/"+syllabus1Id, SyllabusDto.class);
        //then
        assertEquals(NOT_FOUND, responseEntity.getStatusCode());
    }
    @Test
    @DisplayName("when POST a new Syllabus, then returns 201")
    public void givenNewSyllabus_whenPostSyllabus_thenReturns201() {
        //given
        CourseDto defaultcourse = CourseDto.builder().id(1L).build();
        HttpEntity<SyllabusDto> request = new HttpEntity<>(SyllabusDto.builder().ucontent("ABCD").subcode("CS-303").uno("3").uname("data0structure").course(defaultcourse). build());
        //when
        ResponseEntity<Void> responseEntity = restTemplate.postForEntity("/api/v1/syllabus", request, Void.class);
        //then
        assertEquals(CREATED, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getHeaders().getLocation());
    }
    @Test
    @DisplayName("when POST an existing Syllabus id, then returns 422")
    public void givenExistingSyllabusId_whenPostSyllabus_thenReturns422() {
        //given
        Long existingSyllabusId = 1L;
        CourseDto defaultcourse = CourseDto.builder().id(existingSyllabusId).build();
        HttpEntity<SyllabusDto> request = new HttpEntity<>(SyllabusDto.builder().id(existingSyllabusId).ucontent("ABCD").subcode("CS-303").uno("3").uname("data0structure").course(defaultcourse).build());
        //when
        ResponseEntity<Void> responseEntity = restTemplate.postForEntity("/api/v1/syllabus", request, Void.class);
        //then
        assertEquals(UNPROCESSABLE_ENTITY, responseEntity.getStatusCode());
    }
}
