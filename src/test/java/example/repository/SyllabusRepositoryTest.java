package example.repository;
import example.model.Course;
import example.model.Syllabus;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;
import java.math.BigDecimal;
import java.util.Arrays;
import static org.junit.jupiter.api.Assertions.assertEquals;
@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
@Tag("IntegrationTest")
@DisplayName("Syllabus Repository Integration Tests")
public class SyllabusRepositoryTest {
    @Autowired
    private SyllabusRepository syllabusRepository;
    @Autowired
    private CourseRepository courseRepository;
    private Syllabus syllabus1;
    private Syllabus syllabus2;
    private long initialCount;

    @BeforeEach
    public void init() {
        Course course = courseRepository.findById(1L).orElseGet(
                () -> {
                    Course courseAux = Course.builder().subname("data structure").subcode("cs-303").description("ABCDE").prereq("ABCD").build();
                    return courseRepository.save(courseAux);
                });
        syllabus1 = Syllabus.builder().uno("1").uname("data structure").subcode("cs-303").ucontent("ABCDE").course(course).build();
        syllabus2 = Syllabus.builder().uno("2").uname("data structure abcde").subcode("cs-303").ucontent("ABCDEFGhI").course(course).build();
        initialCount = syllabusRepository.count();
        syllabusRepository.saveAll(Arrays.asList(syllabus1, syllabus2));
    }

    @AfterEach
    public void teardown() {
        syllabusRepository.deleteAll(Arrays.asList(syllabus1, syllabus2));
    }

    @Test
    @DisplayName("when deleteById from repository, then deleting should be successful")
    public void whenDeleteByIdFromRepository_thenDeletingShouldBeSuccessful() {
        syllabusRepository.deleteById(syllabus1.getId());
        assertEquals(initialCount + 1, syllabusRepository.count());
    }

    @Test
    @Transactional
    @DisplayName("when delete from derived query, then deleting should be successful")
    public void whenDeleteFromDerivedQuery_thenDeletingShouldBeSuccessful() {
        long deletedRecords = syllabusRepository.deleteBySubcode("cs-303");
        assertEquals(2, deletedRecords);
        assertEquals(initialCount , syllabusRepository.count());
    }

    @Test
    @DisplayName("when deleteAll  from repository, then repository should be restored")
    public void whenDeleteAllFromRepository_thenRepositoryShouldBeRestored() {
        syllabusRepository.deleteAll(Arrays.asList(syllabus1, syllabus2));
        assertEquals(initialCount, syllabusRepository.count());
    }
}