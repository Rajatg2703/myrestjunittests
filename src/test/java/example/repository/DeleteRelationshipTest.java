package example.repository;

import example.model.Course;
import example.model.Syllabus;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import java.awt.print.Book;
import java.math.BigDecimal;
import java.util.Arrays;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
@Tag("IntegrationTest")
@DisplayName("Delete in Relationships Integration Tests")
public class DeleteRelationshipTest {
    @Autowired
    private SyllabusRepository syllabusRepository;
    @Autowired
    private CourseRepository courseRepository;
    private Course course1;
    private Course course2;
    private long courseInitialCount;
    private Syllabus syllabus1;
    private Syllabus syllabus2;
    private long syllabusInitialCount;

    @BeforeEach
    public void init() {
        courseInitialCount = courseRepository.count();
        Course mb1 = Course.builder().id(4l).subcode("cs101").subname("english").description("abbadabbachaabba").prereq("bcb").build();
        course1 = courseRepository.save(mb1);
        Course mb2 = Course.builder().id(5l).subcode("cs102").subname("Chemistry").description("asjf iorjoierjfmieorcjfpmroie").prereq("cbc").build();
        course2 = courseRepository.save(mb2);
        syllabusInitialCount = syllabusRepository.count();
        syllabus1 = Syllabus.builder().id(6l).subcode("cs-303").uname("Data Structure").ucontent("ABCD").uno("1").course(mb1).build();
        syllabus2 = Syllabus.builder().id(7l).subcode("cs-304").uname("Data Structure fdd").ucontent("ABCDef").uno("2").course(mb1).build();

        syllabusRepository.saveAll(Arrays.asList(syllabus1, syllabus2));
    }

    @AfterEach
    public void teardown() {
        syllabusRepository.deleteAll(Arrays.asList(syllabus1, syllabus2));
        courseRepository.deleteAll(Arrays.asList(course1, course2));
    }

    @Test
    @DisplayName("when deleting Courses, then Syllabus should also be deleted too")
    public void whenDeletingCourse_thenSyllabusShouldAlsoBeDeleted() {
        courseRepository.delete(course1);
        courseRepository.delete(course2);
        assertEquals(syllabusInitialCount, syllabusRepository.count());
        assertEquals(courseInitialCount, courseRepository.count());
    }

    @Test
    @DisplayName("when deleting syllabus, then Course should not be deleted")
    public void whenDeletingCourse_thenSyllabusShouldNotBeDeleted() {
        //given
        long courseCount = courseRepository.count();
        syllabusRepository.deleteAll(Arrays.asList(syllabus1, syllabus2));
        assertEquals(syllabusInitialCount, syllabusRepository.count());
        assertEquals(courseCount, courseRepository.count());
    }


}