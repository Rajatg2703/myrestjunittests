package example.service;

import example.dto.CourseDto;
import example.exception.EntityNotFoundException;
import example.model.Course;
import example.repository.CourseRepository;
import org.junit.jupiter.api.*;
import org.modelmapper.ModelMapper;
import org.springframework.cache.annotation.CachePut;
import org.springframework.test.context.ActiveProfiles;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@TestInstance(PER_CLASS)
@ActiveProfiles("test")
@Tag("UnitTest")
@DisplayName("Course Service Unit Tests")
public class CourseServiceTest {

    private CourseRepository courseRepositoryMock;
    private CourseService courseService;


    @BeforeAll
    public void init() {
        courseRepositoryMock = mock(CourseRepository.class);
        courseService = new CourseService(courseRepositoryMock, new ModelMapper());
    }

    @Test
    @DisplayName("given Course data, when create new Course, then Course id is returned")
    void givenCourseData_whenCreateCourse_ThenCourseIdReturned() {

        //given
        CourseDto courseDto1 = CourseDto.builder().subcode("cs101").subname("english").description("abbadabbachaabba").prereq("bcb").build();
        Course course1 = Course.builder().id(0L).subcode("cs101").subname("english").description("abbadabbachaabba").prereq("bcb").build();

        //when
        when(courseRepositoryMock.save(any(Course.class))).thenReturn(course1);
        Long courseId1 = courseService.create(courseDto1);

        //then
        assertNotNull(courseId1);
        assertEquals(course1.getId(), courseId1);
    }

    @Test
    @DisplayName("given Cd incomplete data, when create new Cd, then exception is thrown")
    void givenCdIncompleteData_whenCreateCd_ThenExceptionIsThrown() {

        //given
        CourseDto courseDto1 = CourseDto.builder().subcode("cs101").prereq("bcb").build();
        Course course1 = Course.builder().subcode("cs101").prereq("bcb").build();
        String errorMsg = "Unable to save an incomplete entity : "+courseDto1;

        //when
        when(courseRepositoryMock.save(course1)).thenThrow(new RuntimeException(errorMsg));
        RuntimeException throwException = assertThrows(RuntimeException.class, () ->  courseService.create(courseDto1));

        // then
        assertEquals(errorMsg, throwException.getMessage());
    }

    @Test
    @DisplayName("given Course id, when delete Course, then Course is retrieved")
    void givenCourseId_whenDeleteCourse_ThenCourseRetrieved() {

        //given
        long existingCourseId = 0L;
        Course course1 = Course.builder().id(existingCourseId).subcode("cs101").subname("english").description("abbadabbachaabba").prereq("bcb").build();
        when(courseRepositoryMock.findById(existingCourseId)).thenReturn(Optional.of(course1));

        //when
        CourseDto courseDto1 = courseService.delete(existingCourseId);

        //then
        assertNotNull(courseDto1);
        assertNotNull(courseDto1.getId());
        assertEquals(course1.getId(), course1.getId());
    }

    @Test
    @DisplayName("given Course id, when delete non existing Course, then exception is thrown")
    void givenCourseId_whenDeleteNonExistingCourse_ThenExceptionThrown() {

        //given
        Long nonExistingCourseId = 404L;
        String errorMsg = "Course Not Found : "+nonExistingCourseId;
        when(courseRepositoryMock.findById(nonExistingCourseId)).thenThrow(new EntityNotFoundException(errorMsg));

        //when
        EntityNotFoundException throwException = assertThrows(EntityNotFoundException.class, () ->  courseService.delete(nonExistingCourseId));

        // then
        assertEquals(errorMsg, throwException.getMessage());
    }

}
