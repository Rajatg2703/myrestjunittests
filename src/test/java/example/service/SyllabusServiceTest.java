package example.service;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import example.dto.SyllabusDto;
import example.exception.EntityNotFoundException;
import example.model.Syllabus;
import example.repository.SyllabusRepository;
import org.junit.jupiter.api.*;
import org.modelmapper.ModelMapper;
import org.springframework.test.context.ActiveProfiles;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@TestInstance(PER_CLASS)
@ActiveProfiles("test")
@Tag("UnitTest")
@DisplayName("Syllabus Service Unit Tests")
public class SyllabusServiceTest {

    private SyllabusRepository syllabusRepositoryMock;
    private SyllabusService syllabusService;


    @BeforeAll
    public void init() {
        syllabusRepositoryMock = mock(SyllabusRepository.class);
        syllabusService = new SyllabusService(syllabusRepositoryMock, new ModelMapper());
    }

    @Test
    @DisplayName("given Syllabus id, when get Syllabus, then Syllabus is retrieved")
    void givenSyllabusId_whenGetSyllabus_ThenSyllabusRetrieved() {

        //given
        long existingSyllabusId = 0L;
        Syllabus syllabus1 = Syllabus.builder().id(0L).subcode("cs1001").uno("1").uname("abc").ucontent("aklsjfajkasfjasdhfliuncjkncauie").build();
        when(syllabusRepositoryMock.findById(existingSyllabusId)).thenReturn(Optional.of(syllabus1));

        //when
        SyllabusDto syllabusDto1 = syllabusService.get(existingSyllabusId);

        //then
        assertNotNull(syllabus1);
        assertNotNull(syllabus1.getId());
        assertEquals(syllabusDto1.getId(), syllabus1.getId());
    }

    @Test
    @DisplayName("given Syllabus id, when get non existing Syllabus, then exception is thrown")
    void givenSyllabusId_whenGetNonExistingSyllabus_ThenExceptionThrown() {

        //given
        Long nonExistingSyllabusId = 404L;
        String errorMsg = "Syllabus Not Found : "+ nonExistingSyllabusId;
        when(syllabusRepositoryMock.findById(nonExistingSyllabusId)).thenThrow(new EntityNotFoundException(errorMsg));

        //when
        EntityNotFoundException throwException = assertThrows(EntityNotFoundException.class, () ->  syllabusService.get(nonExistingSyllabusId));

        // then
        assertEquals(errorMsg, throwException.getMessage());
    }

    @Test
    @DisplayName("when list Syllabus, then Syllabus are retrieved")
    void whenListSyllabus_ThenSyllabusRetrieved() {

        //given
        long existingSyllabusId = 0L;
        Syllabus syllabus1 = Syllabus.builder().id(existingSyllabusId).subcode("cs1001").uno("1").uname("abc").ucontent("aklsjfajkasfjasdhfliuncjkncauie").build();
        when(syllabusRepositoryMock.findAll()).thenReturn(Arrays.asList(syllabus1));

        //when
        List<SyllabusDto> syllabus = syllabusService.list();

        //then
        assertNotNull(syllabus);
        assertFalse(syllabus.isEmpty());
        assertEquals(syllabus1.getId(), syllabus.get(0).getId());
    }

    @Test
    @DisplayName("given Syllabus data, when create new Syllabus, then Syllabus id is returned")
    void givenSyllabusData_whenCreateSyllabus_ThenSyllabusIdReturned() {

        //given
        SyllabusDto syllabusDto1 = SyllabusDto.builder().subcode("cs1001").uno("1").uname("abc").ucontent("aklsjfajkasfjasdhfliuncjkncauie").build();
        Syllabus Syllabus1 = Syllabus.builder().id(0L).subcode("cs1001").uno("1").uname("abc").ucontent("aklsjfajkasfjasdhfliuncjkncauie").build();

        //when
        when(syllabusRepositoryMock.save(any(Syllabus.class))).thenReturn(Syllabus1);
        Long syllabusId1 = syllabusService.create(syllabusDto1);

        //then
        assertNotNull(syllabusId1);
        assertEquals(Syllabus1.getId(), syllabusId1);
    }

    @Test
    @DisplayName("given Syllabus incomplete data, when create new Syllabus, then exception is thrown")
    void givenSyllabusIncompleteData_whenCreateSyllabus_ThenExceptionIsThrown() {

        //given
        SyllabusDto syllabusDto1 = SyllabusDto.builder().subcode("cs1001").uno("1").uname("abc").ucontent("aklsjfajkasfjasdhfliuncjkncauie").build();
        Syllabus syllabus1 = Syllabus.builder().subcode("cs1001").uno("1").uname("abc").ucontent("aklsjfajkasfjasdhfliuncjkncauie").build();
        String errorMsg = "Unable to save an incomplete entity : "+syllabusDto1;

        //when
        when(syllabusRepositoryMock.save(syllabus1)).thenThrow(new RuntimeException(errorMsg));
        RuntimeException throwException = assertThrows(RuntimeException.class, () ->  syllabusService.create(syllabusDto1));

        // then
        assertEquals(errorMsg, throwException.getMessage());
    }

}
