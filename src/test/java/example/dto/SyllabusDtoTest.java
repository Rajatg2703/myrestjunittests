package example.dto;

import example.model.Syllabus;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ActiveProfiles("test")
@Tag("UnitTest")
@DisplayName("Syllabus Mapper Unit Tests")
public class SyllabusDtoTest {

    private ModelMapper modelMapper = new ModelMapper();

    @Test
    @DisplayName("when convert Syllabus entity to Syllabus dto, then correct")
    public void whenConvertSyllabusEntityToSyllabusDto_thenCorrect() {

        //given
        Syllabus syllabus = Syllabus.builder().id(1L).subcode("cs1001").uno("1").uname("abc").ucontent("aklsjfajkasfjasdhfliuncjkncauie").build();

        //when
        SyllabusDto syllabusDto = modelMapper.map(syllabus, SyllabusDto.class);

        //then
        assertEquals(syllabus.getId(), syllabusDto.getId());
        assertEquals(syllabus.getUname(), syllabusDto.getUname());
        assertEquals(syllabus.getUcontent(), syllabusDto.getUcontent());
        assertEquals(syllabus.getSubcode(), syllabusDto.getSubcode());

    }

    @Test
    @DisplayName("when convert Syllabus dto to Syllabus entity, then correct")
    public void whenConvertSyllabusDtoToSyllabusEntity_thenCorrect() {

        //given
        SyllabusDto bookDto = SyllabusDto.builder().id(1L).subcode("cs1001").uno("1").uname("abc").ucontent("aklsjfajkasfjasdhfliuncjkncauie").build();

        //when
        Syllabus book = modelMapper.map(bookDto, Syllabus.class);

        //then
        assertEquals(bookDto.getId(), book.getId());
        assertEquals(bookDto.getSubcode(), book.getSubcode());
        assertEquals(bookDto.getUno(), book.getUno());
        assertEquals(bookDto.getUname(),book.getUname());
        assertEquals(bookDto.getUcontent(),book.getUcontent());
    }
}
