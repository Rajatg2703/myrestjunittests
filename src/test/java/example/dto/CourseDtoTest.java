package example.dto;

import example.model.Course;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ActiveProfiles("test")
@Tag("UnitTest")
@DisplayName("Course Mapper Unit Tests")
public class CourseDtoTest {

    private ModelMapper modelMapper = new ModelMapper();

    @Test
    @DisplayName("when convert Course entity to Course dto, then correct")
    public void whenConvertCourseEntityToCourseDto_thenCorrect() {

        //given
        Course course = Course.builder().id(1L).subcode("cs101").subname("english").description("abbadabbachaabba").prereq("bcb").build();

        //when
        CourseDto courseDto = modelMapper.map(course, CourseDto.class);

        //then
        assertEquals(courseDto.getId(), course.getId());
        assertEquals(courseDto.getSubcode(), course.getSubcode());
        assertEquals(courseDto.getSubname(), course.getSubname());
        assertEquals(courseDto.getDescription(), course.getDescription());
    }

    @Test
    @DisplayName("when convert Course dto to Course entity, then correct")
    public void whenConvertCourseDtoToCourseEntity_thenCorrect() {

        //given
        CourseDto courseDto = CourseDto.builder().id(1L).subcode("cs101").subname("english").description("abbadabbachaabba").prereq("bcb").build();

        //when
        Course course = modelMapper.map(courseDto, Course.class);

        //then
        assertEquals(course.getId(), courseDto.getId());
        assertEquals(course.getSubcode() , courseDto.getSubcode());
        assertEquals(course.getSubname(), courseDto.getSubname() );
        assertEquals(course.getDescription() , courseDto.getDescription() );

    }
}
